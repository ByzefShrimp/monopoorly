#include "domain.hpp"
#include <iostream>

class Player
{
private: 
	Domain* playerDomains_;
	float money_;
	int position_; //Depends on the board
public:
	float getMoney() { return money_; }
	void setMoney(float newMoney) { money_ = newMoney; };
	Domain* getDomain() { return playerDomains_; }
	void Pay(Player payTo, float toPay);
	void buyDomain(Domain toBuy);
};